// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE2_Interactadle_generated_h
#error "Interactadle.generated.h already included, missing '#pragma once' in Interactadle.h"
#endif
#define SNAKE2_Interactadle_generated_h

#define Snake2_Source_Snake2_Interactadle_h_13_SPARSE_DATA
#define Snake2_Source_Snake2_Interactadle_h_13_RPC_WRAPPERS
#define Snake2_Source_Snake2_Interactadle_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake2_Source_Snake2_Interactadle_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE2_API UInteractadle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractadle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE2_API, UInteractadle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractadle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE2_API UInteractadle(UInteractadle&&); \
	SNAKE2_API UInteractadle(const UInteractadle&); \
public:


#define Snake2_Source_Snake2_Interactadle_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE2_API UInteractadle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE2_API UInteractadle(UInteractadle&&); \
	SNAKE2_API UInteractadle(const UInteractadle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE2_API, UInteractadle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractadle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractadle)


#define Snake2_Source_Snake2_Interactadle_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteractadle(); \
	friend struct Z_Construct_UClass_UInteractadle_Statics; \
public: \
	DECLARE_CLASS(UInteractadle, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Snake2"), SNAKE2_API) \
	DECLARE_SERIALIZER(UInteractadle)


#define Snake2_Source_Snake2_Interactadle_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake2_Source_Snake2_Interactadle_h_13_GENERATED_UINTERFACE_BODY() \
	Snake2_Source_Snake2_Interactadle_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake2_Source_Snake2_Interactadle_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake2_Source_Snake2_Interactadle_h_13_GENERATED_UINTERFACE_BODY() \
	Snake2_Source_Snake2_Interactadle_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake2_Source_Snake2_Interactadle_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteractadle() {} \
public: \
	typedef UInteractadle UClassType; \
	typedef IInteractadle ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake2_Source_Snake2_Interactadle_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteractadle() {} \
public: \
	typedef UInteractadle UClassType; \
	typedef IInteractadle ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake2_Source_Snake2_Interactadle_h_10_PROLOG
#define Snake2_Source_Snake2_Interactadle_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake2_Source_Snake2_Interactadle_h_13_SPARSE_DATA \
	Snake2_Source_Snake2_Interactadle_h_13_RPC_WRAPPERS \
	Snake2_Source_Snake2_Interactadle_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake2_Source_Snake2_Interactadle_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake2_Source_Snake2_Interactadle_h_13_SPARSE_DATA \
	Snake2_Source_Snake2_Interactadle_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake2_Source_Snake2_Interactadle_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE2_API UClass* StaticClass<class UInteractadle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake2_Source_Snake2_Interactadle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
