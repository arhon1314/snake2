// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake2/Interactadle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInteractadle() {}
// Cross Module References
	SNAKE2_API UClass* Z_Construct_UClass_UInteractadle_NoRegister();
	SNAKE2_API UClass* Z_Construct_UClass_UInteractadle();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Snake2();
// End Cross Module References
	void UInteractadle::StaticRegisterNativesUInteractadle()
	{
	}
	UClass* Z_Construct_UClass_UInteractadle_NoRegister()
	{
		return UInteractadle::StaticClass();
	}
	struct Z_Construct_UClass_UInteractadle_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInteractadle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInteractadle_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Interactadle.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInteractadle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IInteractadle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInteractadle_Statics::ClassParams = {
		&UInteractadle::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UInteractadle_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInteractadle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInteractadle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInteractadle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInteractadle, 1633076661);
	template<> SNAKE2_API UClass* StaticClass<UInteractadle>()
	{
		return UInteractadle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInteractadle(Z_Construct_UClass_UInteractadle, &UInteractadle::StaticClass, TEXT("/Script/Snake2"), TEXT("UInteractadle"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInteractadle);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
