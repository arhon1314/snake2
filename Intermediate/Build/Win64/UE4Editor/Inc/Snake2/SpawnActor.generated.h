// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE2_SpawnActor_generated_h
#error "SpawnActor.generated.h already included, missing '#pragma once' in SpawnActor.h"
#endif
#define SNAKE2_SpawnActor_generated_h

#define Snake2_Source_Snake2_SpawnActor_h_14_SPARSE_DATA
#define Snake2_Source_Snake2_SpawnActor_h_14_RPC_WRAPPERS
#define Snake2_Source_Snake2_SpawnActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake2_Source_Snake2_SpawnActor_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnActor(); \
	friend struct Z_Construct_UClass_ASpawnActor_Statics; \
public: \
	DECLARE_CLASS(ASpawnActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake2"), NO_API) \
	DECLARE_SERIALIZER(ASpawnActor)


#define Snake2_Source_Snake2_SpawnActor_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASpawnActor(); \
	friend struct Z_Construct_UClass_ASpawnActor_Statics; \
public: \
	DECLARE_CLASS(ASpawnActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake2"), NO_API) \
	DECLARE_SERIALIZER(ASpawnActor)


#define Snake2_Source_Snake2_SpawnActor_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnActor(ASpawnActor&&); \
	NO_API ASpawnActor(const ASpawnActor&); \
public:


#define Snake2_Source_Snake2_SpawnActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnActor(ASpawnActor&&); \
	NO_API ASpawnActor(const ASpawnActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnActor)


#define Snake2_Source_Snake2_SpawnActor_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActorToSpawn() { return STRUCT_OFFSET(ASpawnActor, ActorToSpawn); }


#define Snake2_Source_Snake2_SpawnActor_h_11_PROLOG
#define Snake2_Source_Snake2_SpawnActor_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake2_Source_Snake2_SpawnActor_h_14_PRIVATE_PROPERTY_OFFSET \
	Snake2_Source_Snake2_SpawnActor_h_14_SPARSE_DATA \
	Snake2_Source_Snake2_SpawnActor_h_14_RPC_WRAPPERS \
	Snake2_Source_Snake2_SpawnActor_h_14_INCLASS \
	Snake2_Source_Snake2_SpawnActor_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake2_Source_Snake2_SpawnActor_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake2_Source_Snake2_SpawnActor_h_14_PRIVATE_PROPERTY_OFFSET \
	Snake2_Source_Snake2_SpawnActor_h_14_SPARSE_DATA \
	Snake2_Source_Snake2_SpawnActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake2_Source_Snake2_SpawnActor_h_14_INCLASS_NO_PURE_DECLS \
	Snake2_Source_Snake2_SpawnActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE2_API UClass* StaticClass<class ASpawnActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake2_Source_Snake2_SpawnActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
