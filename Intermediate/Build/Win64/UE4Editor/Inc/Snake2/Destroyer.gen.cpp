// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake2/Destroyer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDestroyer() {}
// Cross Module References
	SNAKE2_API UClass* Z_Construct_UClass_ADestroyer_NoRegister();
	SNAKE2_API UClass* Z_Construct_UClass_ADestroyer();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake2();
	SNAKE2_API UClass* Z_Construct_UClass_UInteractadle_NoRegister();
// End Cross Module References
	void ADestroyer::StaticRegisterNativesADestroyer()
	{
	}
	UClass* Z_Construct_UClass_ADestroyer_NoRegister()
	{
		return ADestroyer::StaticClass();
	}
	struct Z_Construct_UClass_ADestroyer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADestroyer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADestroyer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Destroyer.h" },
		{ "ModuleRelativePath", "Destroyer.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ADestroyer_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractadle_NoRegister, (int32)VTABLE_OFFSET(ADestroyer, IInteractadle), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADestroyer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADestroyer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADestroyer_Statics::ClassParams = {
		&ADestroyer::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADestroyer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADestroyer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADestroyer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADestroyer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADestroyer, 1229618723);
	template<> SNAKE2_API UClass* StaticClass<ADestroyer>()
	{
		return ADestroyer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADestroyer(Z_Construct_UClass_ADestroyer, &ADestroyer::StaticClass, TEXT("/Script/Snake2"), TEXT("ADestroyer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADestroyer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
