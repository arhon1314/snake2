// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnActor.h"
#include "Engine/Classes/Components/BoxComponent.h"

// Sets default values 
ASpawnActor::ASpawnActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	
}

// Called when the game starts or when spawned
void ASpawnActor::BeginPlay()
{
	Super::BeginPlay();

	const FRotator SpawnRotation = GetActorRotation();
	FBox Box(FVector(0.f, 0.f, 0.f), FVector(100.f, 100.f, 100.f));
	FVector BoxCenter = Box.GetCenter();
	FVector BoxExtent = Box.GetExtent();
	FVector SpawnLocation = FVector(BoxExtent.X * 2.f, BoxExtent.Y * 2.f, BoxExtent.Z * 2.f);
	GetWorld()->SpawnActor<AActor>(ActorToSpawn, SpawnLocation, SpawnRotation);
}

// Called every frame
void ASpawnActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

