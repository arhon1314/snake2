// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactadle.h"
#include "Destroyer.generated.h"

UCLASS()
class SNAKE2_API ADestroyer : public AActor, public IInteractadle
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADestroyer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
