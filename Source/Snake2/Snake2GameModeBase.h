// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE2_API ASnake2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
